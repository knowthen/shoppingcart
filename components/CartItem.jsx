import React from 'react';
import {formatMoney} from '../format';

function CartItem (props){
  const {item, removeItem, updateQty} = props;
  const itemAmount = item.qty !== 0 ? item.amount / item.qty : 0;
  return (
    <li>
      <span className="glyphicon glyphicon-trash"
        onClick={()=> removeItem(item.code)}/>
      <input
        type='text'
        value={item.qty !== 0 ? item.qty : ''}
        onChange={(e) => updateQty(item.code, e.target.value)}></input>
      {item.name} {item.qty}  {formatMoney(itemAmount)}</li>
  );
}

export default CartItem;
