import React from 'react';
import {formatMoney} from '../format';

function Product (props){
  const {product, addToCart} = props;
  return (
    <li>
      <button onClick={()=> addToCart(product)}>+</button>
      {product.name} <i>{product.promo}</i>
      <b>{formatMoney(product.price(1))}</b>
    </li>
  );
}

export default Product;
