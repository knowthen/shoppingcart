import React from 'react';
import CartItem from './CartItem.jsx';
import {formatMoney} from '../format';

function CartList (props){
  const {cartItems, clearCart, removeItem, updateQty} = props;
  const total = cartItems.reduce( (prev, item) => prev + item.amount, 0)
  return (
      <div>
        <div>Cart Items</div>
        <ul className='list-unstyled'>
          {
            cartItems.map( item => {
              return <CartItem
                        key={item.code}
                        item={item}
                        removeItem={removeItem}
                        updateQty={updateQty} />
            })
          }
        </ul>
        <div><b>Total: {formatMoney(total)}</b></div>
        <button onClick={()=> clearCart()}>Reset Cart</button>
      </div>
    );
}
export default CartList;
