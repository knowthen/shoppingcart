import React from 'react';
import Product from './Product.jsx';

function ProductList(props) {
  const {products, addToCart} = props;
  return (
    <div>
      <div>Products</div>
      <ul className='list-unstyled'>
        {products.map(product => {
          return <Product
                    key={product.code}
                    product={product}
                    addToCart={addToCart}/>
        })
}
      </ul>
    </div>
  );
}

export default ProductList;
